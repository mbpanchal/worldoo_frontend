<?php
/* @var $this UserController */
$dataProvider = 0;
$this->breadcrumbs = array(
    'User' => array('/user'),
    'Profile Update',
);

?>

<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<div class="form">
    <h1>Update Profile Page</h1>
    <form id="confirm_form" name="editprofile_form" action="<?php echo Yii::app()->request->baseUrl; ?>/editprofile" method="post">
        <div class="row">

            <div class="span5 offset5">
                Username : <?php echo $profiledata->username; ?>
            </div>
            <div class="span5 offset5">
                Firstname : <?php echo $profiledata->firstname; ?>
            </div>
            <div class="span5 offset5">
                Lastname : <?php echo $profiledata->lastname; ?>
            </div>
            <div class="span5 offset5">
                Gender : <?php echo $profiledata->gender; ?>
            </div>
            <div class="span5 offset5">
                Date of Birth : <?php echo $profiledata->date_of_birth; ?>
            </div>
            <div class="span5 offset5">
                <input type="text" id="edit_avatar" name="edit_avatar" placeholder="Avtar Image"/>
            </div>
            <div class="span5 offset5">
                <input type="text" id="edit_state" name="edit_state" placeholder="State"/>
            </div>
            <div class="span5 offset5">
                <input type="text" id="edit_city" name="edit_city" placeholder="City"/>
            </div>
            <input type="hidden" id="uid" name="uid" value="<?php echo base64_encode($profiledata->id); ?>" />
            <input type="hidden" id="action_confirmuser" name="action_edituser" value="editprofile_action" />
            <input type="submit" id="sbtconfirmuser" name="editprofile" class="submit_reg_data" value="Update Profile" />

        </div> 
    </form>
</div><!-- form -->