<?php
/* @var $this UserController */
$dataProvider = 0;
$this->breadcrumbs = array(
    'User' => array('/user'),
    'Parent Confirm Page',
);

?>

<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<div class="form">
    <h1>Parent Confirm Page</h1>
    <form id="confirm_form" name="confirm_form" action="<?php echo Yii::app()->request->baseUrl; ?>/confirmuser" method="post">
        <div class="row">

            <div class="span5 offset5">
                <input type="text" id="con_firstname" name="con_firstname" placeholder="Firstname"/>
            </div>
            <div class="span5 offset5">
                <input type="text" id="con_lastname" name="con_lastname" placeholder="Lastname"/>
            </div>
            <div class="span5 offset5">
                <input type="text" id="con_state" name="con_state" placeholder="State"/>
            </div>
            <div class="span5 offset5">
                <input type="text" id="con_city" name="con_city" placeholder="City"/>
            </div>
            <div class="span5 offset5">
                <input type="text" id="con_gender" name="con_gender" placeholder="Gender"/>
            </div>
            <div class="span5 offset5">
                <input type="text" id="con_dob" name="con_dob" placeholder="Date of Birth"/>
            </div>
            <input type="hidden" id="uid" name="uid" value="<?php echo Yii::app()->request->getParam('uid'); ?>" />
            <input type="hidden" id="action_confirmuser" name="action_confirmuser" value="confirmuser_action" />
            <input type="submit" id="sbtconfirmuser" name="sbtconfirmuser" class="submit_reg_data" value="Confirm User" />

        </div> 
    </form>
</div><!-- form -->