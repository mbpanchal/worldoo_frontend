<?php
/* @var $this UserController */

$this->breadcrumbs=array(
	'User',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>

 <?php if (Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
    <?php echo CHtml::encode(Yii::app()->user->getFlash('success')); ?>
</div><!-- /.flash-success -->
<?php endif; ?>


