<?php
/* @var $this UserController */
$dataProvider = 0;
$this->breadcrumbs = array(
    'User' => array('/user'),
    'Register',
);
?>
<?php
// show flash messages  in yii framework Mehul as in 22-1-14
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>

<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<div class="form">
    <h1>Login</h1>
    <form id="login_form" name="login_form" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/user/login" >
        <div class="row">
            <div class="span5 offset5">

                <input type="text" id="reg_username" name="reg_username" placeholder="username" class="wd_username" />
                <div style="display: none; color: red;" id="username_ajax">Loading...</div>
            </div>
            <div class="span5 offset5">
                <input type="text" id="reg_pwd" name="reg_pwd" placeholder="Password"/>
            </div>
            <input type="hidden" id="action_login" name="action_login" value="action_login" />
            <input type="submit" id="sbtlogin" name="sbtlogin" class="login" value="Login" /><div style="display: none; color: green;" id="ajax_reg_submit">Loading...</div>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/user/register" >Are you New User?</a>
        </div> 
    </form>
</div><!-- form -->

<script type="text/javascript">oif 
    $(".submit_reg_data").click(function() {
        var postdata = $("#login_form").serialize();
        $("#ajax_reg_submit").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/user/login',
            dataType: 'json', //html
            data: postdata,
            success: function(result) {
                $("#ajax_reg_submit").html(result.regis_data.result.message);
            },
        });

    });

</script>