<?php
/* @var $this UserController */
$dataProvider = 0;
$this->breadcrumbs = array(
    'User' => array('/user'),
    'Register',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<div class="form">
    <h1>Profile Page</h1>
    <form id="reg_form" name="reg_form" >
        <div class="row">
            <div class="span5 offset5">

                <input type="text" id="reg_username" name="reg_username" placeholder="username" class="wd_username" />
                <div style="display: none; color: red;" id="username_ajax">Loading...</div>
            </div>
            <div id="open_fields" style="display: none;">
                <div class="span5 offset5">
                    <input type="text" id="reg_pwd" name="reg_pwd" placeholder="Password"/>
                </div>
                <div class="span5 offset5">
                    <input type="text" id="reg_rpwd" name="reg_rpwd" placeholder="Repeat Password" />
                </div>
                <div class="span5 offset5">
                    <input type="text" id="reg_pemail" name="reg_pemail" placeholder="Parent's Email Address" />
                </div>
                <input type="hidden" id="action_register" name="action_register" value="register_action" />
                <input type="button" id="sbtregister" name="sbtregister" class="submit_reg_data" value="Register" /><div style="display: none; color: green;" id="ajax_reg_submit">Loading...</div>
            </div>
        </div> 
    </form>
</div><!-- form -->







<script type="text/javascript">

    $(".wd_username").blur(function() {
        //alert('i m calling');
        $("#username_ajax").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/user/checkusername',
            dataType: 'json',
            data: {'action_checkuser': $(this).val()},
            success: function(result) {
                $("#username_ajax").html(result.check_user.message);
                if(result.check_user.flag == 0){
                    $("#open_fields").show();
                }else{
                    $("#open_fields").hide();
                }
            },
        });

    });
    $(".submit_reg_data").click(function() {
        var postdata = $("#reg_form").serialize();
        $("#ajax_reg_submit").show();
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/user/register',
            dataType: 'json', //html
            data: postdata,
            success: function(result) {
                $("#ajax_reg_submit").html(result.regis_data.result.message);
            },
        });

    });

</script>