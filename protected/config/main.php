<?php

//=================Config File=====================
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    //'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Worldoo',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.phpmailer.JPhpMailer', // phpmailer add by mehul as on 21-1-14
    ),
    'language' => 'en', // the language that the user is using and the application should be targeted to  Mehul as on 22-1-14
    'modules' => array(
    // uncomment the following to enable the Gii tool
    /* 		'gii'=>array(
      'class'=>'system.gii.GiiModule',
      'password'=>'mehul',
      // If removed, Gii defaults to localhost only. Edit carefully to taste.
      'ipFilters'=>array('127.0.0.1','::1'),
      ), */
    ),
    // application components
    'components' => array(
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        //'autoUpdateFlash' => false, // add this line to disable the flash counter mehul as on 22-1-14
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => false,
            'rules' => array(
                'checkusername' => 'user/checkusername',
                'register' => 'user/register',
                'login' => 'user/login',
                'logout' => 'user/logout',
                'editprofile' => 'dashboard/editprofile',
                'confirmuser/<uid:[^*]+>' => 'user/confirmuser',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        //session settings
        'session' => array(
            'autoStart' => true, //default true
            //'sessionName' => 'Site Access',
            'cookieMode' => 'only', // with acceptable values of none, allow, and only, equating to: don’t use cookies, use cookies if possible, and only use cookies; defaults to allow
            //'savePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'temp.', //for setting the directory on the server used as the session directory, with a default of /tmp
            'timeout' => '30', //1440 default seconds 
        ),
        /* 'db'=>array(
          'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ), */
        // uncomment the following to use a MySQL database

        /* 'db'=>array(
          'connectionString' => 'mysql:host=localhost;dbname=yii',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => '123456',
          'charset' => 'utf8',
          ), */
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // ‘params’=>include(dirname(__FILE__).‘/params.php’),//<– add as a params file mehul as on 5-1-14
        // API Detail Set Here
        'APP_ID' => 'APP001',
        'APP_KEY' => '28e336ac6c9423d946ba02d19c6a2632',
        'APP_URL' => 'http://192.168.1.109:8888/worldoo_api_live/api/', // bansri's PC API
        //'APP_URL'=>'http://192.168.1.107/worldoo_api_live/api/', // Server API
        //Mailer Settings  Mehul as on 21-1-14
        'Host' => 'smtp.mandrillapp.com',
        'SMTPAuth' => true,
        'Username' => 'bansari@webtechasia.in',
        'Password' => 'tS-MiUbMpKhbDJRJUOkx5Q',
        'SMTPSecure' => 'tls',
        'FormName' => 'Worldoo',
        'FormEmail' => 'noreply@beta.worldoo.com',
    ),
);
