<?php

class UserController extends Controller {
    
    
    public function actionIndex() {
//        Yii::app()->request->getUrl();
//        if (Yii::app()->request->getParam('uid')) {
//            Yii::app()->user->setFlash('success',  Yii::t('wrd_message', 'LABELSELECTTITLE')); 
//
//            Yii::app()->user->setFlash('success', "Data1 saved!");
//            Yii::app()->user->setFlash('error', "Data2 failed!");
//            Yii::app()->user->setFlash('notice', "Data3 ignored.");
//        }
        $this->render('index');
    }

    public function actionCheckusername() {

        $request = Yii::app()->request;
        if ($request->getPost('action_checkuser')) {
            $arrPostData = array(
                'username' => $request->getPost('action_checkuser'),
            );
            $api_obj = new ApiCaller();
            $arrData = $api_obj->sendRequest(array(
                'controller' => 'user',
                'action' => 'checkAvailability',
                'postdata' => $arrPostData,
            ));
            $data = $arrData->result;
            echo CJSON::encode(array('check_user' => $data));
        } else {
            $this->render('register');
        }
    }

    public function actionRegister() {

        $request = Yii::app()->request;
        if ($request->getPost('action_register')) {
            $arrPostData = array(
                'username' => $request->getPost('reg_username'),
                'password' => $request->getPost('reg_pwd'),
                'parent_email' => $request->getPost('reg_pemail'),
                'avatar_img' => 'default_img.jpg',
                'created_ip' => $_SERVER['REMOTE_ADDR'],
            );
            $api_obj = new ApiCaller();
            $arrData = $api_obj->sendRequest(array(
                'controller' => 'user',
                'action' => 'create',
                'postdata' => $arrPostData,
            ));

            if ($arrData->result->flag == 1) {
                $id = base64_encode($arrData->result->response_data);
                $mail = new JPhpMailer;
                $mail->IsSMTP();
                $mail->SetFrom(Yii::app()->params['FormEmail'], Yii::app()->params['FormName']);
                $mail->Subject = Yii::t('wrd_message', 'reg_mail_subect');
                $mail->MsgHTML('<h1>Dear Parents, <br> Varify link <a href="http://192.168.1.107/worldoo_main/confirmuser/' . $id . '" >Verify</a></h1>');
                $mail->AddAddress($request->getPost('reg_pemail'), 'Parents');
                if (!$mail->Send()) {
                    Yii::app()->user->setFlash('error', Yii::t('wrd_message', 'register_error_mail'));
                    //exit;
                } else {
                    //Yii::app()->user->setFlash('success', Yii::t('wrd_message', 'register_scuccess'));
                    //echo CJSON::encode(array('regis_data' => $arrData));
                    echo CHtml::encode($arrData->result->message, true);

                }
            }
        } else {
            $this->render('register');
        }
    }

    public function actionLogin() {

        $request = Yii::app()->request;
        if ($request->getPost('action_login')) {
            $arrPostData = array(
                'username' => $request->getPost('reg_username'),
                'password' => $request->getPost('reg_pwd'),
            );

            $api_obj = new ApiCaller();
            $arrData = $api_obj->sendRequest(array(
                'controller' => 'user',
                'action' => 'checkLogin',
                'postdata' => $arrPostData,
            ));

            if ($arrData->result->flag == 1) {
                //set session
                Yii::app()->session['wrd_userdata'] = $arrData->result->response_data;
                $this->redirect(array('dashboard/index'));
            } else {
                Yii::app()->user->setFlash('error', Yii::t('wrd_message', 'invalid_login'));
                $this->render('login');
            }
        } else {
            $this->render('login');
        }
    }

    public function actionLogout() {
        //destory all session 
        Yii::app()->session->destroy();
        $this->redirect(array('user/index'));
    }

    public function actionConfirmuser() {

        Yii::app()->request->getUrl();
        if (Yii::app()->request->getParam('uid')) {
            $api_obj = new ApiCaller();
            $uid = base64_decode(Yii::app()->request->getParam('uid'));
            //check parent user already confirm or not
            $arrPostData = array('uid' => $uid);
            $arrData = $api_obj->sendRequest(array(
                    'controller' => 'user',
                    'action' => 'readOneById',
                    'postdata' => $arrPostData,
                ));
            if($arrData->result->response_data->activated == 0){
                $this->render('confirmuser');  // if not activated go to confirm form page
            }else{
                $this->redirect(array('user/login'));  // if activated go to login
            }
        }
        $request = Yii::app()->request;
        if ($request->getPost('uid')) {
            if ($request->getPost('action_confirmuser')) {
                $arrPostData = array(
                    'uid' => base64_decode($request->getPost('uid')),
                    'firstname' => $request->getPost('con_firstname'),
                    'lastname' => $request->getPost('con_lastname'),
                    'state' => $request->getPost('con_state'),
                    'city' => $request->getPost('con_city'),
                    'gender' => $request->getPost('con_gender'),
                    'date_of_birth' => $request->getPost('con_dob'),
                    'created_ip' => $_SERVER['REMOTE_ADDR'],
                );
                $api_obj = new ApiCaller();
                $arrData = $api_obj->sendRequest(array(
                    'controller' => 'user',
                    'action' => 'confirmUser',
                    'postdata' => $arrPostData,
                ));
                Yii::app()->user->setFlash('confirm_success', Yii::t('wrd_message', 'confirm_success'));
                $this->render('confirmuser', array('confirm_data' => $arrData));
            } else {
                $this->render('confirmuser');
            }
        }
    }

    public function actionGeneratexml() {

        $module = new TblUsers();
        $events = $module->getallrecords();

        //below array $url converted into required(sitemap.xml) structure       
        $xmldata = '<?xml version="1.0" encoding="utf-8"?>';
        $xmldata .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $xmldata .= '<users>';

        foreach ($events as $url => $data) {
            $xmldata .= '<user>';
            $xmldata .= '<name>' . $data['name'] . '</name>';
            $xmldata .= '<email>' . $data['email'] . '</email>';
            $xmldata .= '<contact_no>' . $data['contact_no'] . '</contact_no>';
            $xmldata .= '<address>' . $data['address'] . '</address>';
            $xmldata .= '<gender>' . $data['gender'] . '</gender>';
            $xmldata .= '<create_date>' . $data['create_date'] . '</create_date>';
            $xmldata .= '</user>';
        }
        $xmldata .= '</users>';
        $xmldata .= '</urlset>';

        if (file_put_contents('sitemap.xml', $xmldata)) {
            echo "sitemap.xml file created on project root folder..";
        }
    }

}
