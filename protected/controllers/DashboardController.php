<?php

class DashboardController extends Controller {

    public function init() {
        // check if login or not if loggin then go ahead with below methods otherwise ask for login
        if (!isset(Yii::app()->session['wrd_userdata'])) {
            $this->redirect(array('user/login'));
        }
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionEditprofile() {

        $request = Yii::app()->request;
        $profiledata = Yii::app()->session['wrd_userdata'];
         
       // if ($request->getPost('uid')) {
            if ($request->getPost('action_edituser')) {
                $arrPostData = array(
                    'uid' => base64_decode($request->getPost('uid')),
                    'state' => $request->getPost('edit_state'),
                    'city' => $request->getPost('edit_city'),
                    'avatar_img' => $request->getPost('edit_avatar'),
                    'created_ip' => $_SERVER['REMOTE_ADDR'],
                );
                $api_obj = new ApiCaller();
                $arrData = $api_obj->sendRequest(array(
                    'controller' => 'user',
                    'action' => 'updateProfile',
                    'postdata' => $arrPostData,
                ));
                Yii::app()->user->setFlash('success', Yii::t('wrd_message', 'editprofile_success'));
                $this->redirect('dashboard/index', array('confirm_data' => $arrData));
            } else {
                $this->render('editprofile',array('profiledata'=>$profiledata));
            }
       // }
    }
    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
    

}
