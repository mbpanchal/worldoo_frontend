<?php
/**
 * ELTSwitch.php
 *
 * Switch between languages and/or themes
 *
 * PHP version 5.1+
 *
 * @author Joe Blocher <yii@myticket.at>
 * @copyright 2012 myticket it-solutions gmbh
 * @license NEW BSD license
 * @version 1.0
 */
class ELTSwitch extends CApplicationComponent
{
    //the keys for session / cookie
    const KEY_THEME = 'eltTheme';
    const KEY_LANGUAGE = 'eltLanguage';

    //used internal
    const TYPE_THEMES = 'themes';
    const TYPE_LANGUAGES = 'languages';

    /**
     * Enable save theme/language in a cookie if $cookieDays > 0
     *
     * @var int
     */
    public $cookieDays = 0;

    /**
     * Allow to switch by url $_GET param if not empty
     *
     * @var string
     */
    public $themeGetParam;

    /**
     * All allowed themes as assoziative array(theme=>name,....)
     * Autodetect of not configured
     *
     * @var array
     */
    public $themes=array();

    /**
     * Set the baseUrl of the ThemeManager if not emtpy
     *
     * @var
     */
    public $baseUrl;

    /**
     * Set the basePath of the ThemeManager if not emtpy
     *
     * @var
     */
    public $basePath;

    /**
     * Allow to switch by url $_GET param if not empty
     *
     * @var string
     */
    public $languageGetParam;

    /**
     * All allowed languages as assoziative array(language=>name,....)
     * Autodetect of not configured
     *
     * @var array
     */
    public $languages=array();

    /**
     * Themes/language keys for specific hosts ($_SERVER['HTTP_HOST'])
     * Not switchable (fixed) if only one key is configured
     * Default is the first listed item
     *
     * 'hosts' => array(
     *   'host1'=>array( //switchabel
     *               'themes'=>array('theme1','theme2',...),
     *               'languages'=>array('de','en',...),
     *               )
     *   'host2'=>array( //fixed to theme2 / en
     *               'themes'=>array('theme2'),  //or 'theme2'
     *               'languages'=>array('en'), //or 'en'
     *               )
     *  )
     *
     * @var array
     */
    public $hosts = array();


    //internal memory cache vars
    protected static $_hostItemNames;
    protected static $_themeNames;
    protected static $_languageNames;

    /**
     * Set the applications theme / language
     */
    public function init()
    {
        parent::init();
        $this->setTheme($this->getTheme());
        $this->setLanguage($this->getLanguage());
    }

    /**
     * Set the current theme
     * Check if allowed, log error if not exists or allowed
     * if empty, remove from cookie / session
     *
     * @param $language
     * @return bool
     */
    public function setTheme($theme)
    {
        $this->setThemeManagerBase();

        if (!empty($theme))
        {
            if ($this->isValidTheme($theme))
            {
                if (empty(Yii::app()->theme) || Yii::app()->theme->name != $theme)
                {
                    Yii::app()->setTheme($theme);

                    if(!$this->isFixedTheme())
                    {
                        //set cookie if $this->cookieDays > 0
                        $this->setCookie(self::KEY_THEME, $theme);

                        //always save to users session too
                        Yii::app()->user->setState(self::KEY_THEME, $theme);
                    }
                }

                return true;

            } else
                Yii::log("Theme '$theme' not configured", 'error');
        } else
            $this->removePersistent(self::KEY_THEME);

        return false;
    }


    /**
     * The long name of the current theme
     *
     * @return string
     */
    public function getThemeName()
    {
       if (!empty(Yii::app()->theme))
       {
           $themes = $this->getThemeNames();
           $key = Yii::app()->theme->name;
           if(isset($themes[$key]))
              return $themes[$key];
       }

       return '';
    }

    /**
     * Set the current language
     * Check if allowed, log error if not exists or allowed
     * if empty, remove from cookie / session
     *
     * @param $language
     * @return bool
     */
    public function setLanguage($language)
    {
        if (!empty($language))
        {
            if ($this->isValidLanguage($language))
            {
                if (empty(Yii::app()->language) || Yii::app()->language != $language)
                {
                    Yii::app()->language = $language;

                    if(!$this->isFixedLanguage())
                    {
                        //set cookie if $this->cookieDays > 0
                        $this->setCookie(self::KEY_LANGUAGE, $language);

                        //always save to users session too
                        Yii::app()->user->setState(self::KEY_LANGUAGE, $language);
                    }

                    return true;
                }

            } else
                Yii::log("Language '$language' not configured", 'error');
        } else
            $this->removePersistent(self::KEY_LANGUAGE);

        return false;
    }

    /**
     * The long name of the current language
     *
     * @return string
     */
    public function getLanguageName()
    {
        if (!empty(Yii::app()->language))
        {
            $languages = $this->getLanguageNames();
            $key = Yii::app()->language;
            if(isset($languages[$key]))
                return $languages[$key];
        }

        return '';
    }


    /**
     * Unset the current theme and remove persist cookie or session
     */
    public function unsetTheme()
    {
        Yii::app()->setTheme(null);
        $this->setTheme(null);

    }

    /**
     * Unset the current language and remove persist cookie or session
     */
    public function unsetLanguage()
    {
        Yii::app()->language = null;
        $this->setLanguage(null);
    }


    /**
     * Get all allowed theme names
     * Can differ by hosts configuration
     *
     * @return array
     */
    public function getThemeNames()
    {
        return $this->getAllowedItemNames(self::TYPE_THEMES);
    }


    /**
     * Get all allowed language names
     * Can differ by hosts configuration
     *
     * @return array
     */
    public function getLanguageNames()
    {
        return $this->getAllowedItemNames(self::TYPE_LANGUAGES);
    }

    /**
     * Check only one theme is configured
     *
     * @return bool
     */
    public function isFixedTheme()
    {
        $items = $this->getThemeNames();
        return !empty($items) && count($items) == 1;
    }

    /**
     * Check only one language is configured
     *
     * @return bool
     */
    public function isFixedLanguage()
    {
        $items = $this->getLanguageNames();
        return !empty($items) && count($items) == 1;
    }

    /**
     * Renders a form with a dropdownList for switching the theme
     * if is not fixed by hosts config
     *
     * @param array $options
     */
    public function renderThemeDropDown($dropdownOptions = array(), $formOptions = array())
    {
        $this->renderDropDownForm(self::TYPE_THEMES,$dropdownOptions,$formOptions);
    }

    /**
     * Renders a form with a dropdownList for switching the language
     * if is not fixed by hosts config
     *
     * @param array $options
     */
    public function renderLanguageDropDown($dropdownOptions = array(), $formOptions = array())
    {
        $this->renderDropDownForm(self::TYPE_LANGUAGES,$dropdownOptions,$formOptions);
    }

    /**
     * Call this method in the init() of your module
     * if you want to change the modules viewPath to 'views/CURRENTTHEME/...'
     *
     * CURRENTTHEME is the Yii::app()->theme->name
     * or the value of a 'theme' property added to your module
     *
     * @param $module
     */
    public static function enableModuleTheme($module)
    {
        if (property_exists($module, 'theme'))
            $theme = $module->theme;
        else
            $theme = !empty(Yii::app()->theme) ? Yii::app()->theme->name : null;

        if (!empty($theme))
            $module->setViewPath($module->getViewPath() . DIRECTORY_SEPARATOR . $theme);
    }


    /**
     * The accessable url of a theme file
     *
     * ELTSwitch::getThemeFileUrl('styles.css','css')
     * Will return the application url /themes/CURRENTTHEME/css/styles.css
     * if the baseUrl property is set to 'themes'
     *
     * @param $file
     * @param string $subDir
     * @param bool $absoluteUrl
     * @return string
     */
    public static function getThemeFileUrl($file, $subDir = '', $absoluteUrl = false)
    {
        $url = empty(Yii::app()->theme) ? Yii::app()->baseUrl : Yii::app()->theme->baseUrl;
        if ($absoluteUrl)
            $url = Yii::app()->createAbsoluteUrl($url);

        if (!empty($subDir))
            $url .= '/' . $subDir;

        return $url . '/' . $file;
    }


    /**
     * Get the CHtml::cssFile from a subDir of the theme baseUrl
     *
     * @param $file
     * @param string $subDir
     * @param string $media
     * @param bool $absoluteUrl
     * @return string
     */
    public static function cssFile($file, $subDir = '',$media='', $absoluteUrl = false)
    {
        return CHtml::cssFile(self::getThemeFileUrl($file,$subDir,$absoluteUrl),$media);
    }

    /**
     * Get the CHtml::scriptFile from a subdirectory of the current theme baseUrl
     *
     * @param $file
     * @param string $subDir
     * @param bool $absoluteUrl
     * @return string
     */
    public static function scriptFile($file, $subDir = '',$absoluteUrl = false)
    {
        return CHtml::scriptFile(self::getThemeFileUrl($file,$subDir,$absoluteUrl));
    }

    /**
     * Get the CHtml::image from a subdirectory of the current theme baseUrl
     *
     * @param $file
     * @param string $subDir
     * @param bool $absoluteUrl
     * @return string
     */
    public static function image($file, $alt='', $htmlOptions=array(),$subDir = 'images', $absoluteUrl = false)
    {
        return CHtml::image(self::getThemeFileUrl($file,$subDir,$absoluteUrl),$alt,$htmlOptions);
    }

    /**
     * Set the ThemeManagers basePath / baseUrl
     */
    protected function setThemeManagerBase()
    {
        if (!empty($this->basePath) || !empty($this->baseUrl))
        {
            $themeManager = Yii::app()->getThemeManager();

            if (!empty($this->basePath))
            {
                $basePath = strpos($this->basePath, '.') === false ? $this->basePath : Yii::getPathOfAlias($this->basePath);
                $themeManager->basePath = $basePath;
            }

            if (!empty($this->baseUrl))
                $themeManager->baseUrl = Yii::app()->createUrl($this->baseUrl);
        }
    }

    /**
     * Render the form with the dropdown to switch theme/language
     *
     * @param $type
     * @param array $dropdownOptions
     */
    protected function renderDropDownForm($type, $dropdownOptions = array(), $formOptions = array())
    {
        $name = $type == self::TYPE_LANGUAGES ? self::KEY_LANGUAGE : self::KEY_THEME;
        $data = $this->getAllowedItemNames($type);

        if (empty($data) || count($data) == 1) //don't display if fixed theme
            return;

        if (!isset($dropdownOptions['submit']))
            $dropdownOptions['submit'] = '';

        if (!isset($dropdownOptions['id']))
            $dropdownOptions['id'] = $name;

        if($type == self::TYPE_LANGUAGES)
           $selected = Yii::app()->getLanguage();
        else
           $selected = !empty(Yii::app()->theme) ? Yii::app()->theme->name : '';

        echo CHtml::form('', 'post', $formOptions);
        echo CHtml::dropDownList($name, $selected, $data, $dropdownOptions);
        echo CHtml::endForm();
    }

    /**
     * Get the allowed items from host or all configured
     *
     * @param $type
     * @return array
     */
    protected function getAllowedItemNames($type)
    {
        $data = $this->getHostItemNames($type);
        if (empty($data)) //not configured in the hosts
            $data = $type == self::TYPE_LANGUAGES ? $this->getAllLanguageNames() : $this->getAllThemeNames();
        return $data;
    }

    /**
     * Check if theme is configured
     *
     * @param $theme
     * @return bool
     */
    protected function isValidTheme($theme)
    {
        return !empty($theme) && in_array($theme, array_keys($this->getThemeNames()));
    }

    /**
     * Check if language is configured
     *
     * @param $language
     * @return bool
     */
    protected function isValidLanguage($language)
    {
        return !empty($language) && in_array($language, array_keys($this->getLanguageNames()));
    }

    /**
     * Get the configured items from a specific host
     *
     * @param $type
     * @return array
     */
    protected function getHostItemNames($type)
    {
        if (isset(self::$_hostItemNames[$type]))
            return self::$_hostItemNames[$type];

        self::$_hostItemNames = array();
        self::$_hostItemNames[$type] = array();

        $host = $_SERVER['HTTP_HOST'];
        $result = array();

        if (isset($this->hosts[$host]) && !empty($this->hosts[$host][$type]))
        {
            $items = is_string($this->hosts[$host][$type]) ? array($this->hosts[$host][$type]) : $this->hosts[$host][$type];
            if (!empty($items))
            {
                $allItems = $type == self::TYPE_THEMES ? $this->getAllThemeNames() : $this->getAllLanguageNames();
                foreach ($items as $item)
                    if (isset($allItems[$item]))
                        $result[$item] = $allItems[$item];
            }
        }

        return self::$_hostItemNames[$type] = $result;
    }

    /**
     * Check a fixed host item
     *
     * @param $type
     * @return mixed|null
     */
    protected function getFixedHostItem($type)
    {
        $items = $this->getHostItemNames($type);

        if (!empty($items) && count($items) == 1)
        {
            reset($items);
            return key($items);
        }

        return null;
    }

    /**
     * Read the cookie
     *
     * @param $attribute
     * @return string
     */
    protected function getCookie($attribute)
    {
        $value = '';
        if (!empty($this->cookieDays))
            $value = isset(Yii::app()->request->cookies[$attribute]) ? Yii::app()->request->cookies[$attribute]->value : '';

        return $value;
    }

    /**
     * Set a cookie
     *
     * @param $attribute
     * @param $value
     */
    protected function setCookie($attribute, $value)
    {
        if (!empty($this->cookieDays) && (int)$this->cookieDays > 0)
        {
            $cookie = new CHttpCookie($attribute, $value);
            $cookie->expire = time() + 60 * 60 * 24 * (int)$this->cookieDays;
            Yii::app()->request->cookies[$attribute] = $cookie;
        }
    }

    /**
     * Remove from session / cookie
     *
     * @param $key
     */
    protected function removePersistent($key)
    {
        if (Yii::app()->user->hasState($key))
            Yii::app()->user->setState($key, null);

        if (isset(Yii::app()->request->cookies[$key]))
            unset(Yii::app()->request->cookies[$key]);
    }

    /**
     * Get all configured themes
     * Autodetect from ThemeManager if property themes is set to null
     *
     * @return array
     */
    protected function getAllThemeNames()
    {
        if (isset($this->themes) && is_array($this->themes))
            return $this->themes;

        //memory cached if autodetect
        if (isset(self::$_themeNames))
            return self::$_themeNames;

        $result = array();
        $names = Yii::app()->getThemeManager()->getThemeNames();
        foreach ($names as $name)
            $result[$name] = $name;

        return self::$_themeNames = $result;
    }

    /**
     * Get the current theme from
     *
     * 1. hosts config if only one is configured
     * 2. $_POST the formvar 'eltTheme'
     * 3. $_GET if property themeGetParam not empty
     * 4. cookie if property cookieDays > 0
     * 5. users session
     * 6. The default theme: first configured in themes property (or host themes)
     *
     * @return mixed|null|string
     */
    protected function getTheme()
    {
        //the fixed, if only one item is configured in hosts
        if (($value = $this->getFixedHostItem(self::TYPE_THEMES)) !== null)
            return $value;

        if (isset($_POST[self::KEY_THEME]) && $this->isValidTheme($_POST[self::KEY_THEME]))
            return $_POST[self::KEY_THEME];

        if (!empty($this->themeGetParam) && isset($_GET[$this->themeGetParam]) && $this->isValidTheme($_GET[$this->themeGetParam]))
            return $_GET[$this->themeGetParam];

        if (($value = $this->getCookie(self::KEY_THEME)) !== '')
           if($this->isValidTheme($value))
            return $value;
           else
            unset(Yii::app()->request->cookies[self::KEY_THEME]); //remove if not valid

        if (Yii::app()->user->hasState(self::KEY_THEME))
        {
            $value = Yii::app()->user->getState(self::KEY_THEME);
            if($this->isValidTheme($value))
                return $value;
            else
                Yii::app()->user->setState(self::KEY_THEME,null); //remove if not valid
        }

        //Default: the first item key
        $allowed = $this->getThemeNames();
        if(!empty($allowed))
        {
            reset($allowed);
            return key($allowed);
        }

        return '';
    }


    /**
     * Get all configured languages
     * Autodetect from Yii::app()->messages->basePath if property languages is set to null
     *
     * @return array
     */
    protected function getAllLanguageNames()
    {
        if (isset($this->languages) && is_array($this->languages))
            return $this->languages;

        //memory cached if autodetect
        if (isset(self::$_languageNames))
            return self::$_languageNames;

        $languages = array();
        $basePath = Yii::app()->messages->basePath;
        $folder = @opendir($basePath);
        while (($file = @readdir($folder)) !== false)
        {
            if ($file !== '.' && $file !== '..' && $file !== '.svn' && $file !== '.gitignore' && is_dir($basePath . DIRECTORY_SEPARATOR . $file))
                $languages[$file] = $file;
        }
        closedir($folder);
        sort($languages);

        return self::$_languageNames = $languages;
    }


    /**
     * Get the current language from
     *
     * 1. hosts config if only one is configured
     * 2. $_POST the formvar 'eltLanguage'
     * 3. $_GET if property themeGetParam not empty
     * 4. cookie if property cookieDays > 0
     * 5. users session
     * 6. The default language: first configured in languages property (or host languages)
     *
     * @return mixed|null|string
     */
    protected function getLanguage()
    {
        //the fixed, if only one item is configured in hosts
        if (($value = $this->getFixedHostItem(self::TYPE_LANGUAGES)) !== null)
            return $value;

        if (isset($_POST[self::KEY_LANGUAGE]) && $this->isValidLanguage($_POST[self::KEY_LANGUAGE]))
            return $_POST[self::KEY_LANGUAGE];

        if (!empty($this->languageGetParam) && isset($_GET[$this->languageGetParam]) && $this->isValidLanguage($_GET[$this->languageGetParam]))
            return $_GET[$this->languageGetParam];

        if (($value = $this->getCookie(self::KEY_LANGUAGE)) !== '')
            if($this->isValidLanguage($value))
                return $value;
            else
                unset(Yii::app()->request->cookies[self::KEY_LANGUAGE]); //remove if not valid

        if (Yii::app()->user->hasState(self::KEY_LANGUAGE))
        {
            $value = Yii::app()->user->getState(self::KEY_LANGUAGE);
            if($this->isValidLanguage($value))
                return $value;
            else
                Yii::app()->user->setState(self::KEY_LANGUAGE,null); //remove if not valid
        }

        //Default: the first item key
        $allowed = $this->getLanguageNames();
        if(!empty($allowed))
        {
            reset($allowed);
            return key($allowed);
        }

        return '';
    }

}
