<?php

class ApiCaller {

    //some variables for the object
    private $_app_id;
    private $_app_key;
    private $_api_url;

    //construct an ApiCaller object, taking an
    //APP ID, APP KEY and API URL parameter
    public function __construct($app_id = null, $app_key = null, $api_url = null) {
        if (!isset($app_id)) {
            $this->_app_id = Yii::app()->params['APP_ID'];
        } else {
            return $app_id;
        }
        if (!isset($app_key)) {
            $this->_app_key = Yii::app()->params['APP_KEY'];
        } else {
            return $app_key;
        }
        if (!isset($api_url)) {
            $this->_api_url = Yii::app()->params['APP_URL'];
        } else {
            return $api_url;
        }
    }

    //send the request to the API server
    //also encrypts the request, then checks
    //if the results are valid
    public function sendRequest($request_params) {


        //encrypt the request parameters
        $enc_request = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->_app_key, json_encode($request_params), MCRYPT_MODE_ECB));

        //create the params array, which will
        //be the POST parameters
        $params = array();
        $params['enc_request'] = $enc_request;
        $params['app_id'] = $this->_app_id;

        //initialize and setup the curl handler
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_api_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute the request
        $objResult = curl_exec($ch);

        //json_decode the result
        $objResult = @json_decode($objResult);

        //echo '<pre>';
        //print_r($objResult);exit;
        if ($objResult == NULL) {
            throw new Exception('API Not working'); exit;
        } else {
            $result = get_object_vars($objResult);
        
            //echo "<pre>"; print_r($result); echo "</pre>";
            //check if we're able to json_decode the result correctly
            if ($result == false || isset($result['success']) == false) {
                throw new Exception('Request was not correct');
            }

            //if there was an error in the request, throw an exception
            if ($result['success'] == false) {
                throw new Exception($result['errormsg']);
            }
            //echo $result['data'];
            //if everything went great, return the data
            if ($result['success'] == 1) {
                return $result['data'];
            }
        }
    }

}
